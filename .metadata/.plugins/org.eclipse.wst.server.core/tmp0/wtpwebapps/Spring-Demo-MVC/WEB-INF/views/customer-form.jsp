<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Registration Form</title>
<style type="text/css">
	.error{color:red}
</style>

</head>
<body>
	<form:form action="processform" modelAttribute="customer">
		
		First name: <form:input path="firstname" />
		<br><br>
		Last name: <form:input path="lastname" />
		<form:errors path="lastname" cssClass="error" />
		<br><br>
		Free Passes: <form:input path="freePass" />
		<form:errors path="freePass" cssClass="error" />
		<br><br>
		
		Postal Code: <form:input path="postalCode" />
		<form:errors path="postalCode" cssClass="error" />
		<br><br>
		
		Course Code: <form:input path="courseCode" />
		<form:errors path="courseCode" cssClass="error" />
		<br><br>
		<input type="submit" value="Submit" />
	</form:form>
</body>
</html>