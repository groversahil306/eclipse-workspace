<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Spring Demo App Title</title>
</head>
<body>
	<h1><a href="showForm">Go to form</a></h1>
	
	<h1><a href="student/showform">Go to student form</a></h1>
	
	<h1><a href="customer/showform">Go to customer form</a></h1>
</body>
</html>