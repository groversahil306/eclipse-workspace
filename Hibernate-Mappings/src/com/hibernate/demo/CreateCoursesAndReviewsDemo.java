package com.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Course;
import com.EntityDemo.Instructor;
import com.EntityDemo.InstructorDetail;
import com.EntityDemo.Review;

public class CreateCoursesAndReviewsDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.addAnnotatedClass(Review.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			session.beginTransaction();
			
			Course c1 = new Course("Spring");
			
			c1.addReview(new Review("I loved it!!!!"));
			c1.addReview(new Review("I loved it toooo!!!!"));
			c1.addReview(new Review("Great Work I loved it!!!!"));
			c1.addReview(new Review("Not a great course!!!!"));
			
			session.save(c1);
			
			
			session.getTransaction().commit();
			System.out.println("Completed");
		}finally {
			session.close();
			factory.close();
		}

	}

}
