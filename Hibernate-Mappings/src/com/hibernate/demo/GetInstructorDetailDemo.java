package com.hibernate.demo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Instructor;
import com.EntityDemo.InstructorDetail;

public class GetInstructorDetailDemo {
	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			session.beginTransaction();
			int id = 3;
			InstructorDetail insdet = session.get(InstructorDetail.class,id);
			Instructor ins = insdet.getInstructor();
			System.out.println(ins);
			session.getTransaction().commit();
			System.out.println("Completed");
				 
		}finally {
			session.close();
			factory.close();
		}

	}

}
