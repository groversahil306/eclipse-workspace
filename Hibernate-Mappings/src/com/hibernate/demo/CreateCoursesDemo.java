package com.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Course;
import com.EntityDemo.Instructor;
import com.EntityDemo.InstructorDetail;

public class CreateCoursesDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			session.beginTransaction();
			int id = 3;
			Instructor ins = session.get(Instructor.class,id);
			Course c1 = new Course("Master of English");
			Course c2 = new Course("Master of Chem");
			ins.add(c1);
			ins.add(c2);
			
			System.out.println("saving....");
			session.save(c1);
			session.save(c2);
			session.getTransaction().commit();
			System.out.println("Completed");
		}finally {
			session.close();
			factory.close();
		}

	}

}
