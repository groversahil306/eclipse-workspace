package com.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Instructor;
import com.EntityDemo.InstructorDetail;

public class DeleteInstructorDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			System.out.println("Deleting object......");
			int id = 1;
			
			session.beginTransaction();
			Instructor ins = session.get(Instructor.class, id);
			System.out.println("Deleting....");
			
			if(ins!=null) {
				session.delete(ins);
			}
			else {
				System.out.println("Could not find Instructor with this id "+id);
			}
			
			session.getTransaction().commit();
			System.out.println("Completed");
				 
		}finally {
			factory.close();
		}

	}

}
