package com.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Course;
import com.EntityDemo.Instructor;
import com.EntityDemo.InstructorDetail;
import com.EntityDemo.Review;
import com.EntityDemo.Student;

public class GetCoursesDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.addAnnotatedClass(Review.class)
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			session.beginTransaction();
			int id = 14;
			Student s = session.get(Student.class,id);
			System.out.println(s.getCourses());
			session.getTransaction().commit();
			System.out.println("Completed");
		}finally {
			session.close();
			factory.close();
		}

	}

}
