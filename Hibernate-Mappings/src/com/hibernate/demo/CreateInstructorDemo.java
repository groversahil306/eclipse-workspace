package com.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Course;
import com.EntityDemo.Instructor;
import com.EntityDemo.InstructorDetail;

public class CreateInstructorDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			System.out.println("Creating object......");
			Instructor i = new Instructor("Sandeep","Khurana","sandeep.Khurana@val.com");
			InstructorDetail id = new InstructorDetail("english","Speech");
			i.setInstructorDetail(id);
			
			session.beginTransaction();
			System.out.println("saving....");
			session.save(i);
			
			session.getTransaction().commit();
			System.out.println("Completed");
				 
		}finally {
			session.close();
			factory.close();
		}

	}

}
