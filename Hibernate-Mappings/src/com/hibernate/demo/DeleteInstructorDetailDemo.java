package com.hibernate.demo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Instructor;
import com.EntityDemo.InstructorDetail;

public class DeleteInstructorDetailDemo {
	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			session.beginTransaction();
			int id = 4;
			InstructorDetail insdet = session.get(InstructorDetail.class,id);
			System.out.println(insdet);
			
			// Remove the associated object reference
			// Break bi-directional link
			
			insdet.getInstructor().setInstructorDetail(null);
			
			// Deleting Now
			session.delete(insdet);
			
			session.getTransaction().commit();
			System.out.println("Completed");
				 
		}
		catch (Exception e) {
			System.out.println(e);
			
		}finally {
			session.close();
			factory.close();
		}

	}

}
