package com.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Course;
import com.EntityDemo.Instructor;
import com.EntityDemo.InstructorDetail;

public class EagerLazyDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			session.beginTransaction();
			int id = 3;
			Instructor ins = session.get(Instructor.class,id);
			System.out.println("This is printing "+ins);
			
			System.out.println("This is printing "+ins.getCourses());
			
			session.getTransaction().commit();
			System.out.println("Completed");
		}finally {
			session.close();
			factory.close();
		}

	}

}
