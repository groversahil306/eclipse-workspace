package com.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Course;
import com.EntityDemo.Instructor;
import com.EntityDemo.InstructorDetail;
import com.EntityDemo.Review;
import com.EntityDemo.Student;

public class CreateCoursesAndStudentsDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.addAnnotatedClass(Review.class)
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			session.beginTransaction();
			Course c1 = new Course("Spring Boot");
			Student s1 = new Student("Sunny","Narang","sannu.narang@gmail.com");
			Student s2 = new Student("Rahul","Jain","rahul.jain@gmail.com");
			session.save(c1);
			c1.addStudent(s1);
			c1.addStudent(s2);
			session.save(s1);
			session.save(s2);
			session.getTransaction().commit();
			System.out.println("Completed");
		}finally {
			session.close();
			factory.close();
		}

	}

}
