package com.springdemo;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.springdemo.validation.CourseCode;

public class Customer {
	
	private String firstname;
	
	@NotNull(message="this field is required")
	@Size(min=1,message="this field is required")
	private String lastname;
	
	@NotNull(message="this field is required")
	@Min(value=0,message="must be greater than 0")
	@Max(value=10,message="must be less than 10")
	private Integer freePass;
	
	
	@NotNull(message="this field is required")
	@Pattern(regexp= "^[0-9]{6}" , message="Input Correct postal code")
	private String postalCode;
	
	
	@NotNull(message="this field is required")
	@CourseCode(value="rakhi")
	private String courseCode;
	
	
	public String getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	public Integer getFreePass() {
		return freePass;
	}
	public void setFreePass(Integer freePass) {
		this.freePass = freePass;
	}
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
}
