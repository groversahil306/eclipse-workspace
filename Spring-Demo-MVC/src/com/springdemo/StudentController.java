package com.springdemo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/student")
public class StudentController {
	
	@RequestMapping("/showform")
	public String showForm(Model model) {
		
		model.addAttribute("student",new Student());
		
		return "showform";
		
	}
	
	@RequestMapping("/processform")
	public String processForm(@ModelAttribute("student") Student s) {
		
		
		return "student-confirmation";
		
	}

}
