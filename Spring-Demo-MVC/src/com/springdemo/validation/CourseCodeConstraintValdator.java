package com.springdemo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CourseCodeConstraintValdator implements ConstraintValidator<CourseCode, String>{

	private String courseprefix;
	
	@Override
	public void initialize(CourseCode constraintAnnotation) {
		
		courseprefix = constraintAnnotation.value();
	}
	
	@Override
	public boolean isValid(String arg0, ConstraintValidatorContext arg1) {
		
		
		boolean result;
		if (arg0 != null)
			result = arg0.startsWith(courseprefix);
		else
			return true;
		
		return result;
	}
	
	
	
	

}
