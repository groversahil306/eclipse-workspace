package com.springdemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:sport.properties")
public class SwimCoach implements Coach {
	
	@Value("${var.email}")
	private String email;
	
	@Value("${var.name}")
	private String name;
	
	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	private FortuneService fortune;
	
	public SwimCoach(FortuneService fortune) {
		this.fortune = fortune;
	}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "Swimming Workout";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortune.getFortune();
	}

}
