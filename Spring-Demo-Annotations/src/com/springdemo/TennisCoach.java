package com.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class TennisCoach implements Coach {

	@Autowired
	@Qualifier("normalFortuneService")
	private FortuneService fortune;
	/*
	@Autowired
	public TennisCoach(FortuneService fortune){
		this.fortune = fortune;
	}
	
	@Autowired
	public void setFortune(FortuneService fortune) {
		this.fortune = fortune;
	}
*/
	
	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "Tennis Workout";
	}
	
	
	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortune.getFortune();
	}

}
