package com.springdemo;

public class BaseballCoach implements Coach{
	
	private FortuneService fortune;
	
	public BaseballCoach(FortuneService fortune) {
		this.fortune = fortune;
	}
	
	public String getDailyWorkout() {
		return "Baseball Workout";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortune.getFortune();
	}
	
	
}
