package com.springdemo;

public class TrackCoach implements Coach {
	
	private FortuneService fortune;
	
	public TrackCoach() {
		
	}
	
	public TrackCoach(FortuneService fortune) {
		this.fortune = fortune;
	}

	@Override
	public String getDailyWorkout() {
		// TODO Auto-generated method stub
		return "Track Coach";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortune.getFortune();
	}

}
