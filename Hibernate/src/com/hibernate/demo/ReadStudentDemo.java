package com.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Student;

public class ReadStudentDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			System.out.println("Creating object......");
			Student obj = new Student("amit","kumar","amit.kumar@val.com");
			
			//Writing Object
			
			session.beginTransaction();
			System.out.println("saving....");
			session.save(obj);
			session.getTransaction().commit();
			
			//Retrieving Object
			System.out.println("Reading Object");
			session = factory.getCurrentSession();
			session.beginTransaction();
			System.out.println("id : " + obj.getId());
			Student ret = session.get(Student.class, obj.getId());
			System.out.println("Get Complete : " + ret);
			session.getTransaction().commit();
			
			
			
			
			System.out.println("Completed");
			
			
			
			
				 
		}finally {
			factory.close();
		}

	}

}
