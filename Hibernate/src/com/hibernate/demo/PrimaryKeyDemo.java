package com.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Student;

public class PrimaryKeyDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Student.class)
				.buildSessionFactory();

		Session session = factory.getCurrentSession();

	try {
	System.out.println("Creating object......");

	Student obj1 = new Student("sahil","grover","sahil.grover@val.com");
	Student obj2 = new Student("Rahul","gupta","rahul.gupta@val.com");
	Student obj3 = new Student("Jay","rana","Jay.rana@val.com");

	session.beginTransaction();

	System.out.println("saving....");
	session.save(obj1);
	session.save(obj2);
	session.save(obj3);

	session.getTransaction().commit();

	System.out.println("Completed");




 
}finally {
factory.close();
}

	}

}
