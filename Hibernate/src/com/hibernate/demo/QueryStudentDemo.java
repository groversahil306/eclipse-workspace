package com.hibernate.demo;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Student;

public class QueryStudentDemo {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			session.beginTransaction();
			
			List<Student> list = session.createQuery("from Student").getResultList();			
			printStudents(list);
			
			// Querying lastname that start from letter g
			System.out.println("\n\nQuerying lastname that start from letter g");
			List<Student> list2 = session.createQuery("from Student s where s.lastname like 'g%'").getResultList();			
			printStudents(list2);
			
			session.getTransaction().commit();
			System.out.println("Completed");
			
		}finally {
			factory.close();
		}

	}

	private static void printStudents(List<Student> list) {
		for (Student s:list)
			System.out.println(s);
	}

}
