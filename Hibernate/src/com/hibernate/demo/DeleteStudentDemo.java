package com.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Student;

public class DeleteStudentDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			int studentId = 6;
			//Retrieving Object
			System.out.println("Reading Object");
			session = factory.getCurrentSession();
			session.beginTransaction();
			System.out.println("id : " + studentId);
			Student ret = session.get(Student.class, studentId);
			System.out.println("Deleting student... ");
			session.createQuery("delete from Student where id=6").executeUpdate();
			session.getTransaction().commit();
			
			System.out.println("Completed");
			
			
			
			
				 
		}finally {
			factory.close();
		}

	}

}
