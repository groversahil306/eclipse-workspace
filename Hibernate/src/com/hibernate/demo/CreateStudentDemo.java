package com.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.EntityDemo.Student;

public class CreateStudentDemo {

	public static void main(String[] args) {
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			System.out.println("Creating object......");
			
			Student obj = new Student("sahil","grover","sahil.grover@val.com");
			
			session.beginTransaction();
			
			System.out.println("saving....");
			session.save(obj);
			
			session.getTransaction().commit();
			
			System.out.println("Completed");
			
			
			
			
				 
		}finally {
			factory.close();
		}

	}

}
